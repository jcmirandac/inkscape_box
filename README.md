# Makercase

Go to:

www.makercase.com

and select the basic box:

![](makercase_001.png)

Set all the options the same way they are in the image (scroll down on makercase to see more options):

![](makercase_002.png)

Then, use the download button, disable panel labels and download the svg file:

![](makercase_003.png)

Go to your downloads folder and right-click the file, then select **open with Inkscape**.

![](box_001.png)

Use the selection tool to select the image an go to **Object** > **ungroup**. You should see each panel is now separated.

![](box_002.png)

Now, create a spiral in one of the panels. At the top, you can change the **Turns**, *Divergence* and **Inner radius**.

![](box_004.png)

On the next panel, create some stars. At the top, you can change **Corners**, **Spoke ratio** and other options.

![](box_005.png)

Then, on another panel, type your name.

![](box_006.png)

Then use the selection tool to select the text, and go to **Path** > **object to path**.

![](box_007.png)

Then, select the panel and the name (use shift key to select more than one object)

![](box_008.png)

Activate the align tools. Then, set **Relative to: biggest object** and center vertically and horizontally.

![](box_009.png)

In the last panel, make any design you want. Then save the file as **svg** and turn it in.
